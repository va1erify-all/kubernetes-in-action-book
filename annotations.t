Помимо меток, модули и другие объекты также могут содержать аннотации.
Аннотации тоже являются парами ключ-значение, поэтому, по сути, они похожи на метки, но не предназначены для хранения идентифицирующей информации. Они не могут использоваться для группирования объектов, так же
как метки. В то время как объекты можно выбирать с помощью селекторов
меток, селектора аннотаций не существует.

С другой стороны, аннотации могут содержать гораздо большие фрагменты
информации и в первую очередь предназначены для использования утилитами. Некоторые аннотации автоматически добавляются в объекты с помощью
Kubernetes, а другие добавляются пользователями вручную.
Аннотации используются при внесении новых функциональных возможностей в Kubernetes. Как правило, альфа- и бета-версии новых функциональных
возможностей не вводят никаких новых полей в объекты API. Вместо полей
используются аннотации, а затем, как только необходимые изменения API
становятся ясными и согласоваными среди разработчиков Kubernetes, вводятся новые поля, и соответствующие аннотации устаревают.
Замечательное применение аннотаций – это добавление описаний для каждого модуля или другого объекта API, чтобы каждый, кто использует кластер, мог
быстро найти информацию о каждом отдельном объекте. Например, аннотация,
используемая для указания имени пользователя, создавшего объект, может значительно упростить взаимодействие между всеми, кто работает в кластере.